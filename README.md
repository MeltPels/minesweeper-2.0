# Minesweeper Game

The Minesweeper Game is a Java-based implementation of the classic Minesweeper game. It provides a graphical user interface (GUI) where players can uncover cells on a grid while avoiding hidden mines. The goal of the game is to uncover all non-mine cells without triggering any mines.

## Features

- Grid-based game board with adjustable width and height.
- Random placement of mines on the game board.
- Cell status updates, revealing adjacent mine counts or indicating the presence of a mine.
- Left-click functionality to uncover cells.
- Right-click functionality to flag potential mine locations.
- Recursive algorithm to uncover adjacent cells with zero adjacent mines.
- Scoreboard to track the current score.
- Reset button to start a new game.

## Getting Started

To run the Minesweeper Game, ensure that you have Java installed on your system. Follow these steps:

1. Clone the repository to your local machine or download the source code files.
2. Open the project in your preferred Java development environment.
3. Compile and run the `MinesweeperGUI.java` file to start the game.
4. The game GUI will appear, allowing you to interact with the game board using left-click and right-click actions.

## Gameplay Instructions

1. Left-click on a cell to uncover its content.
2. If the uncovered cell contains a number, it indicates the number of mines in the adjacent cells.
3. If the uncovered cell contains zero, all adjacent cells without mines will be automatically uncovered.
4. Right-click on a cell to flag it as a potential mine.
5. Uncover all non-mine cells without triggering any mines to win the game.
6. Click the reset button to start a new game.

## Contributing

Contributions to the Minesweeper Game project are welcome. If you find any bugs, have feature suggestions, or want to contribute improvements, please follow these steps:

1. Fork the repository.
2. Create a new branch for your feature or bug fix.
3. Make your changes and ensure they are well-tested.
4. Commit your changes and push the branch to your forked repository.
5. Submit a pull request, explaining your changes and their benefits.

## License

The Minesweeper Game is licensed under the [MIT License](LICENSE).