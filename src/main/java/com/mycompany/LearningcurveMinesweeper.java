/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany;

/**
 *
 * @author Gebruiker
 */
public class LearningcurveMinesweeper {

    Cell[][] mineField;
    boolean gameOver;
    int amBombs;

    public LearningcurveMinesweeper(int rows, int cols, int amBombs) {
        mineField = new Cell[rows][cols];
        makeField(rows, cols);
        addBombs(amBombs);
        this.amBombs = amBombs;
    }

    void openCell(int col, int row) {
        if (!isValidCell(col, row)) return;
        if (mineField[row][col].isRevealed()) return;
        if (isBomb(col,row)){
            openAllBombs();
            gameOver = true;
            return;
        } 
        markRevealed(col,row);
        
        if (amDangerousNeighbours(col,row) == 0) {
            openCell(col-1,row);
            openCell(col+1, row);

            openCell(col-1,row-1);
            openCell(col,row-1);
            openCell(col+1, row-1);

            openCell(col-1,row+1);
            openCell(col,row+1);
            openCell(col+1, row+1);
        }
    }
    
    void markBomb(int col, int row){
        if (isBomb(col,row)){
            markRevealed(col,row);
        }
    }

    String getLabel(int col, int row) {
        if (mineField[row][col].isRevealed()){
            if (isBomb(col,row)){
                return "B";
            }
            if (amDangerousNeighbours(col,row) > 0){
                return Integer.toString(amDangerousNeighbours(col,row));
            }
            return "0";
        }
        return "";
    }
    
    private boolean isBomb(int col, int row) {
        return(mineField[row][col].isBomb());
    }
    
    private void makeField(int rows, int cols) {
        for (int c=0; c < cols; c++){
            for (int r=0; r < rows; r++){
                mineField[r][c] = new Cell(false,false);
            }
        }
    }

    private void addBombs(int amBombs) {
        for (int i = 0; i < amBombs; i++) {
            int newX = (int) Math.floor(Math.random() * mineField.length);
            int newY = (int) Math.floor(Math.random() * mineField[0].length);
            mineField[newY][newX] = new Cell(true, false);
        }
    }

    public boolean isValidCell(int col, int row) {
        return (row >= 0 && row < mineField[0].length && col >= 0 && col < mineField.length);
    }

    public int amDangerousNeighbours(int col, int row) {
        int bommenCounter = 0;
        bommenCounter = bommenCounter
                + amBomb(col - 1, row - 1)
                + amBomb(col, row - 1)
                + amBomb(col + 1, row - 1)
                + amBomb(col + 1, row)
                + amBomb(col + 1, row + 1)
                + amBomb(col, row + 1)
                + amBomb(col - 1, row + 1)
                + amBomb(col - 1, row);
        return bommenCounter;
    }

    public int amBomb(int col, int row) {
        if (!isValidCell(col, row)) {
            return 0;
        }
        return mineField[row][col].anBomb();
    }
    
    private void markRevealed(int col, int row){
        if (!isValidCell(col, row)) return;
        mineField[row][col].markRevealed();
    }

    private void openAllBombs() {
        for (int col=0;col < mineField.length;col++){
            for (int row=0;row < mineField[0].length;row++){
                if (isBomb(col,row)){
                    mineField[row][col].markRevealed();
                }
            }
        }
    }

    double getScore() {
        if (!gameOver){
            double score = 0;
            for (int col=0;col < mineField.length;col++){
                for (int row=0;row < mineField[0].length;row++){
                    if (mineField[row][col].isRevealed()){
                        score++;
                    }
                }
            }
            score = Math.floor(score/(mineField.length*mineField[0].length - amBombs)*100);
            return score;
        }
        return 0;
    }

    public boolean isWinConditionMet() {
        if (!gameOver){
            double score = 0;
            for (int col=0;col < mineField.length;col++){
                for (int row=0;row < mineField[0].length;row++){
                    if (mineField[row][col].isRevealed()){
                        score++;
                    }
                }
            }
            if (score == mineField.length*mineField[0].length - amBombs){
                return true;
            }
        }
        return false;
    }

    public boolean isGameOver(){
        return gameOver;
    }
}
