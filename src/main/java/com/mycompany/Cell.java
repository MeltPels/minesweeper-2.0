/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany;

/**
 *
 * @author Mij
 */
public class Cell {
    
    private final boolean isBomb;
    private boolean isRevealed;
    
    public Cell(boolean isBomb, boolean isRevealed){
        this.isBomb = isBomb;
        this.isRevealed = isRevealed;
    }
    
    public int anBomb(){
        
        if (isBomb){
            return 1;
        }
        return 0;
    }
    
    public boolean isRevealed(){
        return isRevealed;
    }
    
    public void markRevealed(){
        isRevealed = true;
    }

    boolean isBomb() {
        return isBomb;
    }
    
}
